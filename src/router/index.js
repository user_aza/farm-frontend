import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'home',
        component: () => import('../components/Home')
    },
    {
        path: '/report',
        name: 'report',
        component: () => import('../components/Report')
    },
];

const router = new VueRouter({
    mode: 'history',
    routes
});
export default router
